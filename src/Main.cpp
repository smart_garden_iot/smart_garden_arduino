#include <Arduino.h>
#include "Led.h"
#include "Config.h"
#include "BlinkingTask.h"
#include "Scheduler.h"
#include "IOConsole.h"
#include "ManagerTask.h"

Scheduler sched;

auto* redLed = new Led(RED_LED_PIN);
auto* yellowLed = new Led(YELLOW_LED_PIN);
auto* greenLed = new Led(GREEN_LED_PIN);

auto* redLedTask = new BlinkingTask(redLed);
auto* yellowLedTask = new BlinkingTask(yellowLed);
auto* greenLedTask = new BlinkingTask(greenLed);

auto* sleepButton = new Button(BUTTON_PIN);
auto* pot = new Potentiometer(POT_PIN);
auto* flowRegulator = new FlowRegulator(pot);

auto* ioConsole = new IOConsole(
        redLed,
        yellowLed,
        greenLed,
        sleepButton,
        flowRegulator);

auto* msgServiceBt = new MsgServiceBT();
auto* communicator = new Communicator(msgServiceBt);
auto* alarmState = new AlarmState(
        yellowLedTask,
        redLedTask);

auto* managerTask = new ManagerTask(
        ioConsole,
        yellowLedTask,
        greenLedTask,
        alarmState,
        communicator);

void setup() {
    msgServiceBt->init();
    MsgService.init();

    sched.init(50);

    redLedTask->init(800);
    redLedTask->setActive(false);

    yellowLedTask->init(1000);
    yellowLedTask->setActive(false);

    greenLedTask->init(1500);
    greenLedTask->setActive(false);

    managerTask->init(500);

    sched.addTask(managerTask);
    sched.addTask(redLedTask);
    sched.addTask(yellowLedTask);
    sched.addTask(greenLedTask);
}

void loop() {
    sched.schedule();
}
