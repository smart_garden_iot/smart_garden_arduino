#include "Potentiometer.h"

Potentiometer::Potentiometer(int pin){
  this->pin = pin;
} 
  
uint32_t Potentiometer::getValue() const{
    return analogRead(pin);
}
