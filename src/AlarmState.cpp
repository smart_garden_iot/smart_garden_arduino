#include "AlarmState.h"

AlarmState::AlarmState(
        BlinkingTask* yellowLedTask,
        BlinkingTask* redLedTask) {

    this->yellowLedTask = yellowLedTask;
    this->redLedTask = redLedTask;
    redLedTaskActive = false;
    yellowLedTaskActive = false;
}

void AlarmState::onEnter() {

    enableActuators(EnumEvents::blinkRedLed);

    if(yellowLedTaskActive) {
        yellowLedTask->setActive(true);
    }

    if(redLedTaskActive) {
        redLedTask->setActive(true);
    }
}

void AlarmState::enableActuators(EnumEvents event) {
    switch(event) {
        case EnumEvents::blinkRedLed: {
            redLedTaskActive = true;
            break;
        }
        case EnumEvents::blinkYellowLed: {
            yellowLedTaskActive = true;
            break;
        }
    }
}

void AlarmState::onExit() {
    redLedTaskActive = false;
    yellowLedTaskActive = false;

    if(yellowLedTask->isActive()) {
        yellowLedTask->setActive(false);
    }

    if(redLedTask->isActive()) {
        redLedTask->setActive(false);
    }
}
