#include "EnumEvents.h"

const char* getEvent(EnumEvents metric) {
    switch (metric) {
        case EnumEvents::blinkRedLed: return "blinkRedLed";
        case EnumEvents::blinkYellowLed: return "blinkYellowLed";
    }
    return "";
}
