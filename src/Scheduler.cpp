#include "Scheduler.h"

volatile bool timerFlag;

void Scheduler::init(int newBasePeriod) {
    basePeriod = newBasePeriod;
    FlexiTimer2::set(basePeriod, 1.0/1000, [] {
        timerFlag = true;
    });
    FlexiTimer2::start();
    nTasks = 0;
}

bool Scheduler::addTask(Task *task) {
    if (nTasks < MAX_TASKS - 1) {
        taskList[nTasks] = task;
        nTasks++;
        return true;
    } else {
        return false;
    }
}

void Scheduler::schedule() {
    while (!timerFlag){}
    timerFlag = false;

    for (int i = 0; i < nTasks; i++) {
        if (taskList[i]->isActive()) {
            if (taskList[i]->isPeriodic()) {
                if (taskList[i]->updateAndCheckTime(basePeriod)) {
                    taskList[i]->tick();
                }
            } else {
                taskList[i]->tick();
                if (taskList[i]->isCompleted()) {
                    taskList[i]->setActive(false);
                }
            }
        }
    }
}
