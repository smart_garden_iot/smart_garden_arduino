#include "ManagerTask.h"

ManagerTask::ManagerTask(
        IOConsole* ioConsole,
        BlinkingTask* yellowLedTask,
        BlinkingTask* greenLedTask,
        AlarmState* alarmState,
        Communicator* communicator) {

    this->alarmState = alarmState;
    this->yellowLedTask = yellowLedTask;
    this->greenLedTask = greenLedTask;
    this->ioConsole = ioConsole;
    this->communicator = communicator;
    state = EnumState::READY;
    lastFlow = WaterFlow::ONE_QUARTER;
    waterFlow = lastFlow;
    lastSleepQuery = false;
    shouldSleep = lastSleepQuery;
}

void ManagerTask::tick() {
    switch (state) {
        case EnumState::IDLE: {
            state = EnumState::READY;
            attachInterrupt(digitalPinToInterrupt(2), [] {}, RISING);
            ioConsole->sleep();
            detachInterrupt(digitalPinToInterrupt(2));
            Serial.println("$Ready");
            break;
        }
        case EnumState::READY: {
            checkIO();
            if (MsgService.isMsgAvailable()) {
                StateData data{};
                communicator->rcvMsgFromSerial(data);
                Serial.print("$");
                Serial.println(data.topic);
                Serial.print("$");
                Serial.println(data.value);

                if(strcmp(data.topic, "system/state") == 0) {
                    if(strcmp(data.value,
                              getSystemState(EnumState::MONITOR)) == 0) {
                        Serial.println("$Monitor");
                        state = EnumState::MONITOR;
                        greenLedTask->setActive(true);
                    } else if(strcmp(data.value,
                                     getSystemState(
                                             EnumState::WATERING)) == 0) {
                        Serial.println("$Watering");
                        state = EnumState::WATERING;
                        yellowLedTask->setActive(true);
                    } else if(strcmp(data.value,
                                     getSystemState(EnumState::IDLE)) == 0) {
                        Serial.println("$Idle");
                        state = EnumState::IDLE;
                    } else if(strcmp(data.value,
                                     getSystemState(EnumState::ALARM)) == 0) {
                        Serial.println("$Alarm");
                        state = EnumState::ALARM;
                        alarmState->onEnter();
                    }
                }
            }
            if (communicator->isMsgFromBTAvailable()) {
                StateData data{};
                communicator->rcvMsgFromBT(data);
                communicator->sendMsgToSerial(data);
                Serial.println();
//                Serial.println(data.topic);
//                Serial.println(data.value);
            }
            break;
        }
        case EnumState::WATERING: {

            checkIO();
            if(communicator->isMsgFromSerialAvailable()) {
                StateData data{};
                communicator->rcvMsgFromSerial(data);

                if(strcmp(data.topic, "system/state") == 0) {
                    if(strcmp(
                            data.value,
                            getSystemState(EnumState::MONITOR)) == 0) {
                        yellowLedTask->setActive(false);
                        Serial.println("$Monitor");
                        state = EnumState::MONITOR;
                        greenLedTask->setActive(true);
                    } else if(strcmp(
                            data.value,
                            getSystemState(EnumState::IDLE)) == 0) {
                        yellowLedTask->setActive(false);
                        Serial.println("Idle");
                        state = EnumState::IDLE;
                    }
                }
            }
            if (communicator->isMsgFromBTAvailable()) {
                StateData data{};
                communicator->rcvMsgFromBT(data);
                communicator->sendMsgToSerial(data);
                Serial.println();
            }
            break;
        }
        case EnumState::MONITOR: {

            checkIO();
            if(communicator->isMsgFromSerialAvailable()) {
                StateData data{};
                communicator->rcvMsgFromSerial(data);
                if(strcmp(data.topic, "system/state") == 0) {
                    if(strcmp(
                            data.value,
                            getSystemState(EnumState::ALARM)) == 0) {
                        greenLedTask->setActive(false);
                        Serial.println("$Alarm");
                        state = EnumState::ALARM;
                        alarmState->onEnter();
                    } else if(strcmp(
                            data.value,
                            getSystemState(EnumState::IDLE)) == 0) {
                        greenLedTask->setActive(false);
                        Serial.println("$Idle");
                        state = EnumState::IDLE;
                    }  else if(strcmp(data.value,
                                      getSystemState(
                                              EnumState::WATERING)) == 0) {
                        greenLedTask->setActive(false);
                        Serial.println("$Watering");
                        state = EnumState::WATERING;
                        yellowLedTask->setActive(true);
                    }
                }
            }
            if (communicator->isMsgFromBTAvailable()) {
                StateData data{};
                communicator->rcvMsgFromBT(data);
                communicator->sendMsgToSerial(data);
                Serial.println();
            }
            break;
        }
        case EnumState::ALARM: {

            checkIO();
            if(communicator->isMsgFromSerialAvailable()) {
                StateData data{};
                communicator->rcvMsgFromSerial(data);

                if(strcmp(data.topic, "system/state") == 0) {
                    if(strcmp(
                            data.value,
                            getSystemState(EnumState::MONITOR)) == 0) {
                        alarmState->onExit();
                        Serial.println("$Monitor");
                        state = EnumState::MONITOR;
                        greenLedTask->setActive(true);
                    } else if(strcmp(
                            data.value,
                            getSystemState(EnumState::WATERING)) == 0) {
                        alarmState->onExit();
                        Serial.println("$Watering");
                        state = EnumState::WATERING;
                        yellowLedTask->setActive(true);
                    } else if(strcmp(
                            data.value,
                            getSystemState(EnumState::IDLE)) == 0) {
                        alarmState->onExit();
                        Serial.println("$Idle");
                        state = EnumState::IDLE;
                    }
                }
            }
            if (communicator->isMsgFromBTAvailable()) {
                StateData data{};
                communicator->rcvMsgFromBT(data);
                communicator->sendMsgToSerial(data);
                Serial.println();
            }
            break;
        }
    }
}

void ManagerTask::checkIO() {
    ioConsole->getFlowRegulator()->updateFlow();
    waterFlow = ioConsole->getFlowRegulator()->getFlow();
    if(waterFlow != lastFlow) {
        StateData stateData{};
        strlcpy(stateData.topic, "system/waterFlow", sizeof(stateData.topic));
        strlcpy(
                stateData.value,
                ioConsole->getFlowRegulator()->getFlowAsString().c_str(),
                sizeof(stateData.value));
        communicator->sendMsgToSerial(stateData);
        Serial.println();
        lastFlow = waterFlow;
    }

    shouldSleep = ioConsole->shouldSleep();
    if(shouldSleep != lastSleepQuery) {
        StateData stateData{};
        strlcpy(stateData.topic, "system/sleepCmd", sizeof(stateData.topic));
        strlcpy(
                stateData.value, String(shouldSleep).c_str(),
                sizeof(stateData.value));
        communicator->sendMsgToSerial(stateData);
        Serial.println();
        lastSleepQuery = shouldSleep;
    }
    if(shouldSleep) {
        Serial.println("$Idle");
        state = EnumState::IDLE;
    }
}

const char* ManagerTask::getSystemState(EnumState stateValue) {
    switch (stateValue) {
        case EnumState::IDLE: return "0";
        case EnumState::READY: return "2";
        case EnumState::WATERING: return "3";
        case EnumState::MONITOR: return "4";
        case EnumState::ALARM: return "5";
    }
    return "";
}
