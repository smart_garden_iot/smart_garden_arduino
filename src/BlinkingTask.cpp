#include "BlinkingTask.h"

BlinkingTask::BlinkingTask(Led* led) {
    this->led = led;
    state = OFF;
}

void BlinkingTask::setActive(bool active) {
    Task::setActive(active);
    if (active) {
        state = ON;
    } else {
        led->switchOff();
    }
}

void BlinkingTask::tick() {
    switch (state) {
        case ON: {
            led->switchOn();
            state = OFF;
            break;
        }
        case OFF: {
            led->switchOff();
            state = ON;
            break;
        }
    }
}
