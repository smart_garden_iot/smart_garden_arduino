#include "FlowRegulator.h"

FlowRegulator::FlowRegulator(Potentiometer* pot) {
    this->pot = pot;
    flow = WaterFlow::ONE_QUARTER;
    flowString = "none";
}

void FlowRegulator::updateFlow() {
    uint32_t map = pot->getValue();
    if(map >= 0 && map <= 255) {
        flow = WaterFlow::ONE_QUARTER;
    } else if(map >= 256 && map <= 509) {
        flow = WaterFlow::HALF;
    } else if(map >= 510 && map <= 764) {
        flow = WaterFlow::THREE_QUARTER;
    } else if(map >= 765 && map <= 1023){
        flow = WaterFlow::FULL_SPEED;
    }
}

WaterFlow FlowRegulator::getFlow() {
    return flow;
}

String FlowRegulator::getFlowAsString() {
    switch(flow) {
        case WaterFlow::ONE_QUARTER: return "ONE_QUARTER";
        case WaterFlow::HALF: return "HALF";
        case WaterFlow::THREE_QUARTER: return "THREE_QUARTER";
        case WaterFlow::FULL_SPEED: return "FULL_SPEED";
    }
    return "";
}
