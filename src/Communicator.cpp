#include "Communicator.h"

Communicator::Communicator(MsgServiceBT* msgServiceBt) {
    this->msgServiceBt = msgServiceBt;
}

bool Communicator::rcvMsgFromBT(StateData& data) {
    Msg* msg = msgServiceBt->receiveMsg();
    String msgContent = msg->getContent();
    bool status = rcvMsg(data, msgContent);
    delete msg;
    return status;
}

bool Communicator::rcvMsgFromSerial(StateData& data) {
    Msg* msg = MsgService.receiveMsg();
    String msgContent = msg->getContent();
    bool status = rcvMsg(data, msgContent);
    delete msg;
    return status;
}

bool Communicator::rcvMsg(StateData& data, String& input) {
    const size_t capacity = JSON_OBJECT_SIZE(3) + 32;
    StaticJsonDocument<capacity> doc;

    deserializeJson(doc, input);

    strlcpy(data.topic, doc["topic"] | "N/A", sizeof(data.topic));
    strlcpy(data.value, doc["value"] | "N/A", sizeof(data.value));

    return doc.overflowed();
}

bool Communicator::sendMsgToSerial(const StateData& data) {
    return sendMsg(data, Serial);
}

bool Communicator::sendMsg(const StateData& data, Stream& output) {
    const size_t capacity = JSON_OBJECT_SIZE(3);
    StaticJsonDocument<capacity> doc;

    doc["topic"] = data.topic;
    doc["value"] = data.value;

    serializeJson(doc, output);

    return doc.overflowed();
}

bool Communicator::isMsgFromSerialAvailable() {
    return MsgService.isMsgAvailable();
}

bool Communicator::isMsgFromBTAvailable() {
    return msgServiceBt->isMsgAvailable();
}
