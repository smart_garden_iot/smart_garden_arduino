#include "IOConsole.h"

IOConsole::IOConsole(Led* redLed,
                     Led* yellowLed,
                     Led* greenLed,
                     Button* button,
                     FlowRegulator* regulator) {

    this->redLed = redLed;
    this->yellowLed = yellowLed;
    this->greenLed = greenLed;
    sleepButton = button;
    flowRegulator = regulator;
}

void IOConsole::notifyStateChange(const String& state) {
    Serial.println("Scheduled: "+ state);
}

void IOConsole::notifyNewData(const JsonDocument& jsonDocument) {
    serializeJson(jsonDocument, Serial);
}

FlowRegulator* IOConsole::getFlowRegulator() {
    return flowRegulator;
}

bool IOConsole::shouldSleep() {
    return sleepButton->isPressed();
}

void IOConsole::sleep() {
    redLed->switchOff();
    yellowLed->switchOff();
    greenLed->switchOff();

    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_enable();
    sleep_mode();
}
