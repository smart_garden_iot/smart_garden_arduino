#include "MsgServiceBT.h"

void MsgServiceBT::init(){
  content.reserve(256);
  channel.begin(9600);
  availableMsg = nullptr;
}

void MsgServiceBT::sendMsg(Msg msg){
  channel.println(msg.getContent());
}

bool MsgServiceBT::isMsgAvailable(){
  while (channel.available()) {
    char ch = (char) channel.read();
    if (ch == '\n'){
      availableMsg = new Msg(content); 
      content = "";
      return true;    
    } else {
      content += ch;      
    }
  }
  return false;  
}

Msg* MsgServiceBT::receiveMsg(){
  if (availableMsg != nullptr){
    Msg* msg = availableMsg;
    availableMsg = nullptr;
    return msg;  
  } else {
    return nullptr;
  }
}
