#ifndef __MSGSERVICE__
#define __MSGSERVICE__

#include <Arduino.h>
#include "ArduinoJsonWrapper.h"
#include "Msg.h"

class MsgServiceClass {

public:
  Msg* currentMsg;
  bool msgAvailable;
  void init();
  bool isMsgAvailable();
  Msg* receiveMsg();
  void sendMsg(const String& msg);
};

extern MsgServiceClass MsgService;

#endif
