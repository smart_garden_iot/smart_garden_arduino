#ifndef SMART_GARDEN_ARDUINO_ALARMSTATE_H
#define SMART_GARDEN_ARDUINO_ALARMSTATE_H

#include "Vector.h"
#include "Task.h"
#include "BlinkingTask.h"
#include "ArduinoJsonWrapper.h"
#include "Communicator.h"
#include "EnumEvents.h"

class AlarmState {

public:
    AlarmState(
            BlinkingTask* yellowLedTask,
            BlinkingTask* redLedTask);
    void onEnter();
    void onExit();

private:
    BlinkingTask* yellowLedTask;
    BlinkingTask* redLedTask;
    bool yellowLedTaskActive;
    bool redLedTaskActive;

    void enableActuators(EnumEvents event);
};

#endif //SMART_GARDEN_ARDUINO_ALARMSTATE_H
