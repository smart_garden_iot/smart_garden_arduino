#ifndef SMART_GARDEN_ARDUINO_BLINKINGTASK_H
#define SMART_GARDEN_ARDUINO_BLINKINGTASK_H

#include <Arduino.h>
#include "Task.h"
#include "Led.h"

class BlinkingTask : public Task {

public:
    explicit BlinkingTask(Led* led);
    void setActive(bool active) override;
    void tick() override;

private:
    Led* led;
    enum { ON, OFF } state;
};

#endif //SMART_GARDEN_ARDUINO_BLINKINGTASK_H
