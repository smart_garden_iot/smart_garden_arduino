#ifndef SMART_GARDEN_ARDUINO_BUTTON_H
#define SMART_GARDEN_ARDUINO_BUTTON_H

#include <Arduino.h>

class Button {

public:
    explicit Button(int pin);
    bool isPressed() const;

private:
    int pin;
};

#endif //SMART_GARDEN_ARDUINO_BUTTON_H
