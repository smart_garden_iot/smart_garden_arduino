#ifndef SMART_GARDEN_ARDUINO_POTENTIOMETER_H
#define SMART_GARDEN_ARDUINO_POTENTIOMETER_H

#include <Arduino.h>

class Potentiometer {
 
public: 
  Potentiometer(int pin);
  uint32_t getValue() const;

private:
  int pin;

};

#endif //SMART_GARDEN_ARDUINO_POTENTIOMETER_H
