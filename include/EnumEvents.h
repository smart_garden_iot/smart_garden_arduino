#ifndef SMART_GARDEN_ARDUINO_ENUMEVENTS_H
#define SMART_GARDEN_ARDUINO_ENUMEVENTS_H

enum class EnumEvents {
    blinkRedLed,
    blinkYellowLed
};

const char* getEvent(EnumEvents metric);

#endif //SMART_GARDEN_ARDUINO_ENUMEVENTS_H
