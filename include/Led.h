#ifndef SMART_GARDEN_ARDUINO_LED_H
#define SMART_GARDEN_ARDUINO_LED_H

#include <Arduino.h>

class Led {

public:
    explicit Led(int pin);
    void switchOn() const;
    void switchOff() const;

private:
    int pin;
};

#endif //SMART_GARDEN_ARDUINO_LED_H
