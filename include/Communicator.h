#ifndef SMART_GARDEN_ARDUINO_COMMUNICATOR_H
#define SMART_GARDEN_ARDUINO_COMMUNICATOR_H

#include "ArduinoJsonWrapper.h"
#include "MsgService.h"
#include "MsgServiceBT.h"
#include "StateData.h"

class Communicator {

public:
    explicit Communicator(MsgServiceBT* msgServiceBt);
    bool sendMsgToSerial(const StateData& data);
    bool rcvMsgFromSerial(StateData& data);
    bool rcvMsgFromBT(StateData& data);
    bool isMsgFromSerialAvailable();
    bool isMsgFromBTAvailable();

private:
    bool sendMsg(const StateData& data, Stream& output);
    bool rcvMsg(StateData& data, String& input);
    MsgServiceBT* msgServiceBt;
};

#endif //SMART_GARDEN_ARDUINO_COMMUNICATOR_H
