#ifndef SMART_GARDEN_ESP8266_IOCONSOLE_H
#define SMART_GARDEN_ESP8266_IOCONSOLE_H

#include "ArduinoJsonWrapper.h"
#include "FlowRegulator.h"
#include "Button.h"
#include "Led.h"
#include <avr/sleep.h>

class IOConsole {

public:
    IOConsole(
            Led* redLed,
            Led* yellowLed,
            Led* greenLed,
            Button* button,
            FlowRegulator* regulator);

    static void notifyStateChange(const String& state);
    static void notifyNewData(const JsonDocument& jsonDocument);
    FlowRegulator* getFlowRegulator();
    bool shouldSleep();
    void sleep();

private:
    Led* redLed;
    Led* yellowLed;
    Led* greenLed;
    Button* sleepButton;
    FlowRegulator* flowRegulator;
};

#endif //SMART_GARDEN_ESP8266_IOCONSOLE_H
