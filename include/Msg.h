#ifndef SMART_GARDEN_ARDUINO_MSG_H
#define SMART_GARDEN_ARDUINO_MSG_H

class Msg {
    String content{};

public:
    explicit Msg(const String& content){
        this->content = content;
    }

    String getContent(){
        return content;
    }
};


#endif //SMART_GARDEN_ARDUINO_MSG_H
