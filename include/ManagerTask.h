#ifndef SMART_GARDEN_ESP8266_TASKMANAGER_H
#define SMART_GARDEN_ESP8266_TASKMANAGER_H

#include "IOConsole.h"
#include "Task.h"
#include "MsgService.h"
#include "Communicator.h"
#include "AlarmState.h"
#include "BlinkingTask.h"
#include "StateData.h"

class ManagerTask: public Task {

public:
    ManagerTask(
            IOConsole* ioConsole,
            BlinkingTask* yellowLedTask,
            BlinkingTask* greenLedTask,
            AlarmState* alarmState,
            Communicator* communicator);

    void tick() override;

private:
    enum class EnumState {
        IDLE,
        READY,
        WATERING,
        MONITOR,
        ALARM
    };

    void checkIO();
    const char* getSystemState(EnumState stateValue);

    AlarmState* alarmState;
    BlinkingTask* yellowLedTask;
    BlinkingTask* greenLedTask;
    Communicator* communicator;
    IOConsole* ioConsole;
    EnumState state;
    WaterFlow waterFlow;
    WaterFlow lastFlow;
    bool shouldSleep;
    bool lastSleepQuery;
    const uint32_t standardSleepTime = 3600;
};

#endif //SMART_GARDEN_ESP8266_TASKMANAGER_H
