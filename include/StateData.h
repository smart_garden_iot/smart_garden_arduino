#ifndef SMART_GARDEN_ARDUINO_STATEDATA_H
#define SMART_GARDEN_ARDUINO_STATEDATA_H

struct StateData {
    char topic[32];
    char value[16];
};

#endif //SMART_GARDEN_ARDUINO_STATEDATA_H
