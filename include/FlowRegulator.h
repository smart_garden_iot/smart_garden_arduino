#ifndef SMART_GARDEN_ARDUINO_FLOWREGULATOR_H
#define SMART_GARDEN_ARDUINO_FLOWREGULATOR_H

#include <Arduino.h>
#include "Potentiometer.h"

enum class WaterFlow{
    ONE_QUARTER, HALF, THREE_QUARTER, FULL_SPEED
} ;

class FlowRegulator {

public:
    explicit FlowRegulator(Potentiometer* pot);
    void updateFlow();
    String getFlowAsString();
    WaterFlow getFlow();

private:
    Potentiometer* pot;
    WaterFlow flow;
    String flowString;

};

#endif //SMART_GARDEN_ARDUINO_FLOWREGULATOR_H
