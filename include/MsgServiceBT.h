#ifndef SMART_GARDEN_ARDUINO_MSGSERVICEBT_H
#define SMART_GARDEN_ARDUINO_MSGSERVICEBT_H

#include "Arduino.h"
#include "AltSoftSerial.h"
#include "Msg.h"

class MsgServiceBT {
    
public: 
  MsgServiceBT() = default;
  void init();
  bool isMsgAvailable();
  Msg* receiveMsg();
  void sendMsg(Msg msg);

private:
  String content{};
  Msg* availableMsg{};
  AltSoftSerial channel;
  
};

#endif //SMART_GARDEN_ARDUINO_MSGSERVICEBT_H
